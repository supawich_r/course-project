package view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import model.Character;
import model.Keys;
import model.ball;

import java.util.ArrayList;

public class Platform extends Pane {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;
    public static final int GROUND = 350;
    public static final int CENTERLEFT = 395;
    public static final int CENTERRIGHT = 405;
    public static final int CENTER = 400;


    private Image platformImg;
    private ArrayList<Character> characterList;
    private ArrayList<Character> characterList02;

    private ArrayList<ball> ballList;
    private ArrayList<ball> ballList02;

    private ArrayList<Score> scoreList;
    private Keys keys;
    private Character character;
    private Character character02;
    private ball ball;
    private ball ball2;


    public Platform() {
        characterList = new ArrayList<>();

        ballList = new ArrayList<>();

        scoreList = new ArrayList();
        keys = new Keys();
        ball = new ball(400,20,0,0);
        ball2 = new ball(400,20,0,0);


        platformImg = new Image(getClass().getResourceAsStream("/assets/Backgroundwow.png"));
        ImageView backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        characterList.add(new Character(30, 30,0,268, KeyCode.A,KeyCode.D,KeyCode.W ,KeyCode.SPACE,KeyCode.R ,KeyCode.F,KeyCode.NUMPAD1));
        characterList.add(new Character(Platform.WIDTH-60, 30,0,268, KeyCode.LEFT,KeyCode.RIGHT,KeyCode.UP,KeyCode.SHIFT,KeyCode.BACK_SPACE,KeyCode.NUMPAD0,KeyCode.G));
        scoreList.add(new Score(30,GROUND + 20));
        scoreList.add(new Score(Platform.WIDTH-60,GROUND + 20));
        getChildren().add(backgroundImg);
        getChildren().addAll(characterList);
        getChildren().addAll(scoreList);
        getChildren().addAll(ball);
        getChildren().addAll(ball2);


    }
    public ball getball() {
        return ball;
    }

    public ball getball2() {
        return ball2;
    }

    public static int random() {
        int min = 0;
        int max = 800;
        int x = (int)(Math.random() * (max - min + 1) + min);
        return x;
    }
    public ArrayList<Character> getCharacterList() {
        return characterList;
    }

    public ArrayList<ball> getBallList() {
        return ballList;
    }


    public ArrayList<Character> getCharacterList02() {
        return characterList02;
    }

    public Keys getKeys() {
        return keys;
    }

    public ArrayList<Score> getScoreList() {
        return scoreList;
    }

    public Character getCharacter() { return character; }
    public Character getCharacter02() { return character02; }



}

