package model;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class AnimatedSprite extends ImageView{

    int count, columns, offsetX, offsetY, width, height, curXIndex=0, curYIndex=0;

    public AnimatedSprite(Image image, int count, int columns, int offsetX, int offsetY, int width, int height){
        this.setImage(image);
        this.count = count;
        this.columns = columns;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.width = width;
        this.height = height;
        this.setViewport(new Rectangle2D(offsetX, offsetY, width, height));
    }

    public void tick() {
        curXIndex = (curXIndex+1)%columns;
        curYIndex = (curYIndex+1)/columns;
        interpolate();
    }

    public void tick02() {
        curXIndex = (curXIndex+1)%columns;
        curYIndex = (curYIndex+1)/columns;
        interpolate02();
    }

    public void tick03() {
        curXIndex = (curXIndex+1)%columns;
        curYIndex = (curYIndex+1)/columns;
        interpolate03();
    }


    protected void interpolate() {
        final int x = curXIndex*width+offsetX;
        final int y = curYIndex*height+offsetY;
        this.setViewport(new Rectangle2D(x, y, width, height));
    }


    protected void interpolate02() {
        final int x = curXIndex*width+offsetX-1;
        final int y = curYIndex*height+offsetY+63;
        this.setViewport(new Rectangle2D(x, y, width, height));
    }


    protected void interpolate03() {
        final int x = curXIndex*width+offsetX-1;
        final int y = curYIndex*width+offsetY+130;
        this.setViewport(new Rectangle2D(x, y, width, height));

    }


}
