package model;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import view.Platform;
import java.util.Random;

public class ball extends Pane {
    public static final int CHARACTER_WIDTH = 65;
    public static final int CHARACTER_HEIGHT = 65;
    private int x;
    private int y;
    private int temp;
    private int startX;
    private int startY;
    private int offsetX;
    private int offsetY;
    private int score = 0;
    private Image characterImg;
    private AnimatedSprite imageView;
    private boolean isFalling = true;
    private boolean isHit = false;
    private boolean ishitFloor = false;

    public ball(int x, int y, int offsetX, int offsetY) {
        this.x = x;
        this.y = y;
        this.startX = x;
        this.startY = y;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.characterImg = new Image(getClass().getResourceAsStream("/assets/ballball.png"));
        this.imageView = new AnimatedSprite(characterImg, 4, 4, offsetX, offsetY, 42, 42);
        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
        this.getChildren().addAll(this.imageView);
    }


    public void moveY() {
        setTranslateY(y);
        setTranslateX(x);
        if (isFalling) {
            y = y + 4;
            x = x + temp;
        }
    }

    public boolean getIsHitFloor() {
        return ishitFloor;
    }

    public void moveX() {
        setTranslateX(x);
    }

    public void hitByCharacter() {
        setTranslateY(y);
        setTranslateX(x);
        y = y - 5;
        x = x - temp;
        isFalling = false;
    }

    public void paint() {
        moveX();
        moveY();
    }

    public void paint2() {
        moveX();
        moveY();
    }

    public void checkReachFloor() {
        if (isFalling && y >= Platform.GROUND - CHARACTER_HEIGHT ) {
            isFalling = false;
        }
    }

    public void checkReachGameWall() {
        if (x <= 0) {
            Random rand = new Random();
            temp = rand.nextInt(10)%2 == 0 ? -3:3;;
            isHit = false;
            isFalling = true;
            ishitFloor = false;
        }else if (x + getWidth() >= Platform.WIDTH)
        {
            Random rand = new Random();
            temp = rand.nextInt(10)%2 == 0 ? -3:3;
            isHit = true;
            isFalling = false;
            ishitFloor = false;
        }
        if(y <= 0){
            Random rand = new Random();
            temp = rand.nextInt(10)%2 == 0 ? -3:3;
            isHit = false;
            isFalling = true;
            ishitFloor = false;
        }else if(y + getHeight() >= Platform.GROUND){
            Random rand = new Random();
            temp = rand.nextInt(10)%2 == 0 ? -3:3;
            isHit = true;
            isFalling = false;
            ishitFloor = true;
        }
    }

    public void checkCollisionWithCharacter(Character a){
        if(this.getBoundsInParent().intersects(a.getBoundsInParent())){
            Random rand = new Random();
            temp = rand.nextInt(10)%2 == 0 ? -10:10;
            isHit = true;
        }
    }

    public boolean isHit(){
        return isHit;
    }

    public AnimatedSprite getImageView(){
        return imageView;
    }

    public int getX(){
        return x;
    }
}

