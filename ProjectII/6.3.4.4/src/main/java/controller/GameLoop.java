package controller;

import model.Character;
import view.Platform;
import view.Score;


import java.util.ArrayList;

public class GameLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public GameLoop(Platform platform) {
        this.platform = platform;
        frameRate = 5;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void update(ArrayList<Character> characterList) {

        for (Character character : characterList ) {
            if (platform.getKeys().isPressed(character.getLeftKey()) || platform.getKeys().isPressed(character.getRightKey())) {
                character.getImageView().tick();
            }

            if (platform.getKeys().isPressed(character.getLeftKey())) {
                character.setScaleX(-1);
                character.moveLeft();
                character.trace();
                character.settofive();
            }

            if (platform.getKeys().isPressed(character.getRightKey())) {
                character.setScaleX(1);
                character.moveRight();
                character.trace();
                character.settofive();
            }
            if (platform.getKeys().isPressed(character.getIncreaseKey())){
                character.increaseSpeed();
            }
            if (platform.getKeys().isPressed(character.getDecreaseKey())){
                character.decreaseSpeed();
            }

            if (!platform.getKeys().isPressed(character.getLeftKey()) && !platform.getKeys().isPressed(character.getRightKey()) ) {
                character.stop();
            }

            if (platform.getKeys().isPressed(character.getUpKey())) {
                character.jump();
                character.getImageView().tick02();
                character.trace();
            }
            if (platform.getKeys().isPressed(character.getspaceKey())) {
//                character.jump();
                character.getImageView().tick03();
                character.trace();
                character.specialattack();
            }
            if (platform.getKeys().isPressed(character.getrKey())) {

                character.respawn();
                character.trace();
            }
        }
    }

    private void updateScore(ArrayList<Score> scoreList, ArrayList<Character> characterList) {
        javafx.application.Platform.runLater(() -> {
            for (int i=0 ; i<scoreList.size() ; i++) {
                scoreList.get(i).setPoint(characterList.get(i).getScore());


            }
        });
    }

    private void updateSpeed(ArrayList<Score> speedList, ArrayList<Character> characterList) {
        javafx.application.Platform.runLater(() -> {
            for (int i=0 ; i<speedList.size() ; i++) {
                speedList.get(i).setPoint(characterList.get(i).getSpeed01());
                speedList.get(i).setPoint(characterList.get(i).getSpeed02());
            }
        });
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();

            update(platform.getCharacterList());
            updateScore(platform.getScoreList(),platform.getCharacterList());
            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
