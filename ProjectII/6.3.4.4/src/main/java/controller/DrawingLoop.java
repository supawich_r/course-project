package controller;

import model.Character;
import model.ball;
import view.Platform;

import java.util.ArrayList;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        frameRate = 60;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void checkDrawCollisions(ArrayList<Character> characterList, ball getball , ball getball2)  {
        for (Character character : characterList ) {
            character.checkReachGameWall();
            character.checkReachHighest();
            character.checkReachFloor();
            //character.checkReachPost();
            character.checkReachCenter();
            //character.checkReachCenter2();
            getball.checkCollisionWithCharacter(character);
        }
        for (Character cA : characterList) {
            for (Character cB : characterList) {
                if( cA != cB) {
                    if (cA.getBoundsInParent().intersects(getball.getBoundsInParent())) {
                        cA.addtouchtime();
                        //cA.addspeed();
//                        getball.spawnball();
                    }
                }

                if( cB != cA) {
                    if (cB.getBoundsInParent().intersects(getball.getBoundsInParent())) {
                        cB.addtouchtime();
                        //cB.addspeed();
//                        getball.spawnball();
                    }
                }
            }
            getball.checkReachFloor();
            getball.checkReachGameWall();

            getball2.checkReachFloor();
            getball2.checkReachGameWall();
        }
    }

    private void paint(ArrayList<Character> characterList) {
        for (Character character : characterList ) {
            character.repaint();
        }
    }

    private void paint(ball ball) {
        ball.paint();
    }

    private void paint2(ball ball2) {
        ball2.paint2();
    }

    private void hit(ball ball) {
        ball.hitByCharacter();
    }

    private void hit2(ball ball2) {
        ball2.hitByCharacter();
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();
            checkDrawCollisions(platform.getCharacterList(),platform.getball(),platform.getball2());
            paint(platform.getCharacterList());
            if(platform.getball().isHit()){
                hit(platform.getball());
                hit2(platform.getball2());
            }else{
                paint(platform.getball());
                paint2(platform.getball2());
            }
            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
