import controller.DrawingLoop;
import controller.GameLoop;
import model.Character;
import javafx.scene.input.KeyCode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.saxsys.mvvmfx.testingutils.jfxrunner.JfxRunner;
import view.Platform;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

@RunWith(JfxRunner.class)
public class CharacterTest {
    private Character floatingCharacter;
    private Character floatingCharacter02;
    private ArrayList<Character> characterListUnderTest;
    private Platform platformUnderTest;
    private GameLoop gameLoopUnderTest;
    private DrawingLoop drawingLoopUnderTest;
    //private CollidedLoop collidedLoopTest;
    private Method updateMethod;
    private Method redrawMethod;
    private Method checkcollided;

    @Before
    public void setup() {
        floatingCharacter = new Character(30, 30, 0, 0, KeyCode.A, KeyCode.D, KeyCode.W, KeyCode.SPACE,KeyCode.R,KeyCode.F,KeyCode.NUMPAD1);
        floatingCharacter02 = new Character(30, 30, 0, 0, KeyCode.A, KeyCode.D, KeyCode.W, KeyCode.SHIFT,KeyCode.BACK_SPACE,KeyCode.NUMPAD0,KeyCode.G);
        characterListUnderTest = new ArrayList<Character>();
        characterListUnderTest.add(floatingCharacter);
        characterListUnderTest.add(floatingCharacter02);
        platformUnderTest = new Platform();
        gameLoopUnderTest = new GameLoop(platformUnderTest);
        drawingLoopUnderTest = new DrawingLoop(platformUnderTest);

        try {
            updateMethod = GameLoop.class.getDeclaredMethod("update", ArrayList.class);
            redrawMethod = DrawingLoop.class.getDeclaredMethod("paint", ArrayList.class);
            updateMethod.setAccessible(true);
            redrawMethod.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            updateMethod = null;
            redrawMethod = null;
            checkcollided = null;

        }
    }

    @Test
    public void characterInitialValuesShouldMatchConstructorArguments() {
        assertEquals("Initial x", 30, floatingCharacter.getX(), 0);
        assertEquals("Initial y", 30, floatingCharacter.getY(), 0);
        assertEquals("Offset x", 0, floatingCharacter.getOffsetX(), 0.0);
        assertEquals("Offset y", 0, floatingCharacter.getOffsetY(), 0.0);
        assertEquals("Left key", KeyCode.A, floatingCharacter.getLeftKey());
        assertEquals("Right key", KeyCode.D, floatingCharacter.getRightKey());
        assertEquals("Up key", KeyCode.W, floatingCharacter.getUpKey());
        assertEquals("Space key", KeyCode.SPACE, floatingCharacter.getspaceKey());
    }

    @Test
    //Left
    public void characterShouldMoveToTheLeftAfterTheLeftKeyIsPressed() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startX = characterUnderTest.getX();
        platformUnderTest.getKeys().add(KeyCode.A);
        updateMethod.invoke(gameLoopUnderTest,characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest,characterListUnderTest);
        Field isMoveLeft = characterUnderTest.getClass().getDeclaredField("isMoveLeft");
        isMoveLeft.setAccessible(true);
        assertTrue("Controller: Left key pressing is acknowledged",platformUnderTest.getKeys().isPressed(KeyCode.A));
        assertTrue("Model: Character moving left state is set", isMoveLeft.getBoolean(characterUnderTest));
        assertTrue("View: Character is moving left", characterUnderTest.getX() < startX);
    }

    @Test
    //01
    //Add a test case to test the consequences after pressing the key D on the keyboard.
    public void characterShouldMoveToTheRightAfterTheRightKeyIsPressed() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startX = characterUnderTest.getX();
        platformUnderTest.getKeys().add(KeyCode.D);
        updateMethod.invoke(gameLoopUnderTest,characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest,characterListUnderTest);
        Field isMoveRight = characterUnderTest.getClass().getDeclaredField("isMoveRight");
        isMoveRight.setAccessible(true);
        assertTrue("Controller: Right key pressing is acknowledged",platformUnderTest.getKeys().isPressed(KeyCode.D));
        assertTrue("Model: Character moving right state is set", isMoveRight.getBoolean(characterUnderTest));
        assertTrue("View: Character is moving right", characterUnderTest.getX() > startX);
    }


    @Test
    //03
    //Add a test case to test the consequence after pressing the key W on the keyboard. Let the character is not being on the ground.
    public void characterShouldMoveToTheJumpAfterTheJumpKeyIsPressed() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startY = characterUnderTest.getY();
        platformUnderTest.getKeys().add(KeyCode.W);
        updateMethod.invoke(gameLoopUnderTest,characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest,characterListUnderTest);
        Field isMoveUp = characterUnderTest.getClass().getDeclaredField("falling");
        isMoveUp.setAccessible(true);
        assertTrue("Controller: Jump key pressing is acknowledged",platformUnderTest.getKeys().isPressed(KeyCode.W));
        assertTrue("Model: Character moving up state is set", isMoveUp.getBoolean(characterUnderTest));
        assertTrue("View: Character is moving up", characterUnderTest.getY() > startY);
    }

    @Test
    //04
    //Test actions
    //Add a test case to test the consequences as the character is hitting a border
    public void characterShouldMoveToTheHittingAfterTheHittingIsPressed() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startY = characterUnderTest.getY();
        updateMethod.invoke(gameLoopUnderTest,characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest,characterListUnderTest);
        Field hitting = characterUnderTest.getClass().getDeclaredField("hitting");
        hitting.setAccessible(true);
        assertTrue("Model: Character hitting state is set", hitting.getBoolean(characterUnderTest));
        assertTrue("View: Character is hitting", characterUnderTest.CHARACTER_WIDTH <= platformUnderTest.WIDTH);
    }

    @Test
    //collidedBall
    public void characterShouldCollidedTheBall() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);

        if (characterUnderTest.getX() == 150 && characterUnderTest.getY() == 150) {
            assertEquals(characterUnderTest.isCollidedBall(),true);
        }
    }

    //Test score
    //ข้อ 5
    @Test
    public void characterShouldGet1PointWhenCollidedTheBall() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);

        if (characterUnderTest.isCollidedBall() == true) {
            assertEquals(characterUnderTest.getScore(),1);
        }
    }
}









